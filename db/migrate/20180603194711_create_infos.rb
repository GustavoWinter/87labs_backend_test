class CreateInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :infos do |t|
      t.string :city
      t.string :country
      t.date :date
      t.string :genre

      t.timestamps
    end
  end
end
