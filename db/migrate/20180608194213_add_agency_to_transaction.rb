class AddAgencyToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :from_agency, :integer
  end
end
