Rails.application.routes.draw do
    post 'auth/login', to: 'authentication#authenticate'
    post 'signup', to: 'users#create'
    resources :users, only: [ :destroy, :index, :show, :update ]

    get '/transactions/type/:transaction_type' => 'transactions#show_by_type'
    get '/transactions/type/:transaction_type/:account_id' => 'transactions#show_by_to_user'
    resources :transactions, ony: [ :create, :index, :show ]

    resources :infos, only: [ :destroy, :create, :update, :index, :show ]
    resources :accounts, only: [ :update, :index, :show, :destroy, :create ] do
      get '/bank_statement' => 'accounts#bank_statement'
    end
    get '/accounts/balance/:id' => 'accounts#balance'
end
