class User < ApplicationRecord
  has_secure_password

  has_many :infos, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :transactions

  validates_presence_of :name, :cpf,:password_digest

  validates :cpf, uniqueness: true

end
