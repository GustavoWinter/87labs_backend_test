class Transaction < ApplicationRecord
  belongs_to :user
  belongs_to :account

  validates_presence_of :value, :transaction_type
end
