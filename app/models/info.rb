class Info < ApplicationRecord
  belongs_to :user

  validates_presence_of :city, :country, :date, :genre
end
