class AccountsController < ApplicationController
  before_action :set_user_account, except: [:create, :index, :bank_statement]
  before_action :account_params, only: [:update_limit]

  def index
    @accounts = @current_user.accounts
    json_response(@accounts)
  end

  def show
    json_response(@account)
  end

  def create
    account = @current_user.accounts.create!(generate_user_account)
    json_response(account, :created)
  end

  def update
    if @account.limit_time.nil? || ((Time.now - @account.limit_time) >= 600)
      @account.update(limit_time: Time.now, limit: params[:limit])
        head :no_content
    elsif ((Time.now - @account.limit_time) < 600)
      render status:202, json: {
        message: "You only can update the limit of this account",
        error: "Not updated, you have wait 10 minutes to update your limit again."
      }
    end
  end

  def balance
    balance = @account
    json_response(balance)
  end

  def bank_statement
    statement = Transaction.select("transaction_type, value, user_id, account_id, from_agency, created_at")
                          .where("created_at >= ? AND user_id = ? AND account_id = ?", (Time.now - 7.days), @current_user.id, params[:account_id])
                          .order(created_at: :desc)
    arr = [];
    statement.each do |t|
      h = {
            type: t.transaction_type,
            value: t.value,
            from: User.find(t.user_id).name,
            from_agency: t.from_agency,
            to: Account.find(t.account_id).user.name,
            make_at: t.created_at
          }
      arr.push(h)
    end
    json_response(arr)
  end

  def destroy
    @account.destroy
    head :no_content
  end

  private

  def set_user_account
    @account = @current_user.accounts.find(params[:id])
  end



  def account_params
    params.permit(:limit)
  end

  def generate_user_account
    account = {}
    random_number = Random.new
    account_number = random_number.rand(99999)
    agency = random_number.rand(9999)
    balance = random_number.rand(1800.00).round(2)
    account[:agency] = agency
    account[:number] = account_number
    account[:balance] = balance
    account[:limit] = 800
    account[:limit_time] = Time.now
    return account
  end

end
