require 'bigdecimal'
require 'bigdecimal/util'

class TransactionsController < ApplicationController
  before_action :withdraw_notes, only: [:create]
  before_action :by_type, only: [:show_by_type]
  before_action :create_params, only: [:create]

  def create
    from_account = Account.find_by(agency: params[:from_agency])
    to_account = Account.find(params[:account_id])
    transaction = @current_user.transactions.create(create_params)
    if !from_account.nil?
      if to_account.transactions << transaction
        if update_by_type(params[:transaction_type], to_account, from_account)
          json_response(transaction, 201)
        end
      end
    else
      raise(ExceptionHandler::InvalidAgency, Message.invalid_agency)
    end
  end

  def index
    transaction = @current_user.transactions
    if transaction.empty?
      json_response({message: "This user does not have transactions"})
    else
      json_response(transaction)
    end
  end

  def show
    transaction_to = Transaction.select("value, user_id, account_id, transaction_type, from_agency, id")
                                      .where("user_id = ? AND id = ?", @current_user.id, params[:id])
                                      .order(created_at: :desc)
    if transaction_to != []
      json_response(transaction_to)
    else
      json_response({message: "Transaction with this id not found"}, 422)
    end
  end

  def show_by_type
    if check_if_contains(params[:transaction_type])
      transaction_by_type = Transaction.select("value, transaction_type, user_id, account_id")
                                        .where("user_id = ? AND transaction_type = ?", @current_user.id, params[:transaction_type])
                                        .order(created_at: :desc)
      json_response(transaction_by_type, 200)
    else
      json_response({
        message: "Invalid transaction type"
        }, 404)
    end
  end

  def show_by_to_user
    if check_if_contains(params[:transaction_type]) && Account.find(params[:account_id])
      transaction_by_type = Transaction.select("value, transaction_type, user_id, account_id, account_id")
                                       .where("user_id = ? AND transaction_type = ? AND account_id = ?", @current_user.id, params[:transaction_type], params[:account_id])
                                       .order(created_at: :desc)
      json_response(transaction_by_type, 200)
    else
      json_response({
        message: "Invalid transaction type or account does not exist"
        }, 404)
    end
  end


  private
  $notes = nil

  def by_type
    params.permit(:transaction_type)
  end

  def create_params
    params.permit(:value, :transaction_type, :account_id, :from_agency)
  end

  def check_if_contains(type)
    array = ["deposit", "transfer", "withdraw"]
    array.include?(type.downcase)
  end

  def withdraw_notes
    if($notes.nil? && params[:transaction_type].downcase == "withdraw" && params[:value] != "0")
      $notes = [notes, params[:value]]
      json_response({message:"You need to specify the notes you want by id number", example: "notes=0 OR notes=1", availables: "notes=0..6", notes: $notes})
    elsif  params[:value] == nil || params[:value] == "0"
      raise(ExceptionHandler::InvalidNoteValue, Message.invalid_note_value)
    end
  end

  def reset_notes(arg=true)
    $notes = nil
    if arg
      json_response({message: "Your withdraw action was reset, now you can insert a new value"})
    end
  end

  def last_digit(number)
    number_to_str = number.to_s
    str_number = number_to_str[-1]
    return_last_digit = str_number.to_i
  end

  def notes
    params.permit(:value)
    value = params[:value].to_i
    last_digit = last_digit(value)
    new_value = value - last_digit
    hash = {}
    bank_notes = [100, 50, 20, 10, 5, 2]
      bank_notes.each_with_index do |item, index|
        h = { "#{index}" => []}
        h["#{index}"].push("Value = R$#{value}")
        if new_value % item == 0 && new_value >= item
          h["#{index}"].push("#{new_value/item} x R$#{item}")
        else
          rest = new_value % item
          copy = bank_notes.dup.slice(index..-1)
          for i in copy do
            if !(rest % i == 0) && i != 100
              h["#{index}"].push("#{new_value / item} x  R$#{item}")
              if !(rest / i == 0)

                h["#{index}"].push("#{rest/i} x R$#{i}")
              end
              if(bank_notes.include?(rest % i))
                number = copy.index(rest % i)
                h["#{index}"].push("1 x R$#{rest % i}")
              end
            end
            copy.delete_at(i)
          end
        end
        if !(last_digit == 0)
          if last_digit.odd? && last_digit > 5
            h["#{index}"].push("1 x R$5")
            h["#{index}"].push("#{(last_digit-5)/2} x R$2")
          elsif last_digit.even?
            h["#{index}"].push("#{last_digit/2} x R$2")
          elsif last_digit == 5
            h["#{index}"].push("1 x R$5")
          elsif last_digit == 3
            h["#{index}"].push("1 x R$2")
            h["#{index}"].push("1 x R$1 ")
          elsif last_digit.even?
            h["#{index}"].push("#{last_digit % 2} x  R$cinco")
          elsif last_digit == 1
            h["#{index}"].push("1 real")
          end
          if params[:value].to_d % 1 != 0
            h["#{index}"].push("Sorry, we don't have coins, #{params[:x].to_d - value} is not processed.")
          end
        end
        hash.merge!(h)
      end
    hash
  end

  def update_by_type(type, current_account, from_account)
    deposit_transfer = current_account.balance + params[:value].to_d
    withdraw = current_account.balance - params[:value].to_d
    transfer = from_account.balance - params[:value].to_d
    case type.downcase
    when "deposit"
      if !(from_account.limit >= params[:value].to_d && from_account.id == current_account.id)
        raise(ExceptionHandler::InvalidDeposit, Message.invalid_deposit)
      else
        current_account.update_attribute(:balance, deposit_transfer)
      end
    when "transfer"
      if !(from_account.balance >= params[:value].to_d) || from_account.id == current_account.id
        raise(ExceptionHandler::InvalidTransfer, Message.invalid_transfer)
      end
      from_account.update_attribute(:balance, transfer)
      current_account.update_attribute(:balance, deposit_transfer)
    when "withdraw"
      if $notes[0].has_key?("#{params[:note]}")
        if (from_account.balance.to_d > params[:value].to_d && from_account.id == current_account.id)
          reset_notes(false)
          from_account.update_attribute(:balance, withdraw)
          true
        else
          raise(ExceptionHandler::InvalidWithdraw, Message.invalid_withdraw)
        end
      end
    else
     return false
    end
  end
end
