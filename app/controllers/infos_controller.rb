class InfosController < ApplicationController
  before_action :set_user_info, except: [:create, :index]


  def index
    @infos = @current_user.infos
    json_response(@infos)
  end

  def show
    info = @current_user.infos.find(params[:id])
    json_response(info)
  end

  def create
      info = @current_user.infos.create!(info_params)
      json_response(info, :created)
  end


  def update
    @info.update(info_params)
  end

  def destroy
    @info.destroy
    head :no_content
  end

  private

  def set_user_info
    @info = Info.find(params[:id])
  end

  def info_params
    params.permit(:city, :country, :date, :genre)
  end

end
