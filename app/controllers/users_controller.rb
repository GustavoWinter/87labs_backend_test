class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create


  def index
    json_response(User.all)
  end

  def show
    json_response(@current_user)
  end

  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.cpf, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  def update
    @current_user.update(user_params)
    head :no_content
  end

  def destroy
    @current_user.destroy
    head :no_content
  end

  private
    def user_params
      params.permit(:name, :cpf, :password, :password_confirmation)
    end

    def delete_params
      params.permit(:cpf, :password, :password_confirmation)
    end
end
