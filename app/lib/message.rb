class Message
  def self.not_found(record = 'record')
    "Sorry, #{record} not found."
  end

  def self.invalid_credentials
    'Invalid credentials'
  end

  def self.invalid_token
    'Invalid token'
  end

  def self.missing_token
    'Missing token'
  end

  def self.unauthorized
    'Unauthorized request'
  end

  def self.account_created
    'Account created successfully'
  end

  def self.account_not_created
    'Account could not be created'
  end

  def self.expired_token
    'Sorry, your token has expired. Please login to continue.'
  end

  def self.invalid_agency
    'This agency is invalid.'
  end

  def self.invalid_deposit
    'The deposit limit is R$800. The account id must be the same as the user.'
  end

  def self.invalid_transfer
    'You need to have a bigger or equal balance to the value you want to transfer. Transfer could not be make to the same account. Use deposit instead.'
  end

  def self.invalid_withdraw
    'Withdraw must be make from the same account as the user who request that. Your balance must be bigger or equal to the value.'
  end

  def self.invalid_genre
    'The genre must be Male or Female.'
  end

  def self.invalid_note_value
    'The value could not be blank or 0.'
  end
end
