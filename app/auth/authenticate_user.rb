class AuthenticateUser
  def initialize(cpf, password)
    @cpf = cpf
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_reader :cpf, :password

  def user
    user = User.find_by(cpf: cpf)
    return user if user && user.authenticate(password)
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end
