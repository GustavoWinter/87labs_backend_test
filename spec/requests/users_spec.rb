require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }
  let(:user) { users.first}
  let(:valid) { valid_headers }
  let(:headers) { valid_headers.except('Authorization') }
  let(:valid_attributes) do
    attributes_for(:user, name: "Carl Sagan", password_confirmation: user.password)
  end

  describe 'POST /signup' do
    context 'when valid request' do
      before { post '/signup', params: valid_attributes.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns success message' do
        expect(json['message']).to match(/Account created successfully/)
      end

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when invalid request' do
      before { post '/signup', params: {}, headers: headers }

      it 'does not create a new user' do
        expect(response).to have_http_status(422)
      end

      it 'returns failure message' do
        expect(json['message'])
          .to match(/Validation failed: Password can't be blank, Name can't be blank, Cpf can't be blank, Password digest can't be blank/)
      end
    end
  end


  describe 'GET /users' do
    before { get '/users', params: {}, headers: valid }

    it 'returns users' do
      expect(json.count).to eq(10)
      expect(json).not_to be_empty
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /users' do
    before { get "/users", params: {}, headers: valid }

    context 'When the record exits' do
      it 'returns the user' do
        expect(json).not_to be_empty
      end
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'DELETE /users/:id' do
    before { delete "/users/#{user_id}", params: {}, headers: valid }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
