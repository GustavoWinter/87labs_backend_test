require 'rails_helper'

RSpec.describe 'Accounts API', type: :request do
  let!(:user) { create(:user) }
  let!(:accounts) { create_list(:account, 10, user_id: user.id) }
  let(:id) { accounts.first.id}
  let(:headers) { valid_headers }


  describe 'GET /accounts' do
    before { get "/accounts", params: {}, headers: headers }

    context 'when account exist' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns user accounts' do
        expect(json.count).to eq(10)
      end
    end
  end

  describe 'get /accounts/:account_id' do
    before { get "/accounts/#{id}", params: {}, headers: headers }

    context 'when user account exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it "returns the account" do
        expect(json['id']).to eq(id)
      end
    end

    context 'when user account does not exist' do
      let(:id) { 0 }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Account with 'id'=0 /)
      end
    end
  end  

  describe 'PUT /accounts/:account_id' do

    context 'when limit time is ok' do
      let(:valid_attributes) { { limit: 1000 } }
      before { put "/accounts/#{id}", params: valid_attributes.to_json, headers: headers}

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it "updates the account limit" do
        updated_account = Account.find(id)
        updated_account.update(limit: 1000)
        expect(updated_account.limit.to_i).to match(1000)
      end
    end
  end

  describe 'DELETE /accounts/:account_id' do
    before { delete "/accounts/#{id}", headers: headers }

    it "returns status code 204" do
      expect(response).to have_http_status(204)
    end
  end
end
