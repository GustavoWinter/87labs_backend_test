require 'rails_helper'

RSpec.describe 'Transactions API', type: :request do
  let!(:user) { create(:user) }
  let!(:account) { create(:account, user_id: user.id) }
  let!(:transaction) { create(:transaction, user_id: user.id, account_id: account.id, from_agency: account.agency) }
  let(:transaction_type) { transaction.transaction_type}
  let(:account_id) { account.id }
  let(:headers) { valid_headers }


  describe ' GET /transactions'  do
    before { get "/transactions", params: {}, headers: headers}

    context 'when transactions exist' do
      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end

      it "returns transactions" do
        expect(json[0]['id']).to eq(transaction.id)
      end
    end
  end

  describe '/transactions/type/:transaction_type' do
    before { get "/transactions/type/#{transaction_type}", params: {}, headers: headers }

    context 'when user transactions type exists' do
        it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it "returns the transactions type" do
        expect(json[0]['id']).to eq(nil)
        expect(json[0]['transaction_type']).to eq(transaction_type)
        expect(json[0]['user_id']).to eq(user.id)
      end
    end

    context 'when user transactions type does not exist' do
      let(:transaction_type) { "Null" }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Invalid transaction type/)
      end
    end
  end

  describe '/transactions/type/:type/:account_number' do
    before { get "/transactions/type/#{transaction_type}/#{account_id}", params: {}, headers: headers }

    context 'when user transactions type exists to that user' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it "returns the transactions type" do
        expect(json[0]['id']).to eq(nil)
        expect(json[0]['transaction_type']).to eq(transaction_type)
        expect(json[0]['user_id']).to eq(user.id)
        expect(json[0]['account_id']).to eq(account_id)
      end
    end

    context 'when user does not have the transactions type to that user' do
      let(:transaction_type) { "Null" }

      it "returns status code 404" do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Invalid transaction type or account does not exist./)
      end
    end
  end

  describe 'POST /transactions' do
    context 'when the transfer is valid exists' do
      let(:second_user) { create(:account, user_id: user.id) }
      let(:valid_attributes) { { value: 1, transaction_type: "Transfer", account_id: second_user.id, from_agency: account.agency } }
      before { post "/transactions", params: valid_attributes.to_json, headers: headers }

      it "returns status code 201" do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the deposit is valid' do
      let(:valid_attributes) { { value: 1, transaction_type: "Deposit", account_id: account_id, from_agency: account.agency } }
      before { post "/transactions", params: valid_attributes.to_json, headers: headers }

      it "returns status code 201" do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the witdraw is valid exists' do
      let(:valid_attributes) { { value: 1, transaction_type: "Withdraw", account_id: account_id, from_agency: account.agency, note: 0 } }
      before { post "/transactions", params: valid_attributes.to_json, headers: headers }

      it "returns status code 200" do
        expect(response).to have_http_status(200)
      end
    end

    context 'when an invalid agency' do
      let!(:invalid_attributes) { { value: 50, transaction_type: "Deposit", account_id: account_id, from_agency:50000 }}
      before { post "/transactions", params: invalid_attributes.to_json, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/This agency is invalid./)
      end
    end

    context 'when an invalid request' do
      let!(:invalid_attributes) { { value: 990, transaction_type: "Deposit", account_id: 0, from_agency:12345 }}
      before { post "/transactions", params: invalid_attributes.to_json, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(404)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Couldn't find Account with 'id'=0/)
      end
    end
  end

end
