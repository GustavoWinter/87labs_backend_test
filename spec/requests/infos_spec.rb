require 'rails_helper'

RSpec.describe 'Infos API', type: :request do
  let!(:user) { create(:user) }
  let!(:infos) { create_list(:info, 10, user_id: user.id) }
  let(:info_id) { infos.last.id }
  let(:headers) { valid_headers }

  describe 'GET /infos' do
    before { get "/infos", params: {}, headers: headers }

    context 'when info exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns user infos' do
        expect(json.size).to eq(10)
      end
    end
  end

  describe 'get /infos/:info_id' do
    before { get "/infos/#{info_id}", params: {}, headers: headers }

    context 'when user info exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the info' do
        expect(json['id']).to eq(info_id)
      end
    end

    context 'when user info does not exist' do
      let(:info_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Info/)
      end
    end
  end

  describe '/infos' do
    let(:valid_attributes) { {
        city: 'Portland',
        country: 'USA',
        date: Time.now,
        genre: "Male"
      }
    }

    context 'when request attributes are valid' do
      before { post "/infos", params: valid_attributes.to_json, headers: headers }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      let(:invalid_attributes) { { city: 12, country: 0 } }
      before { post "/infos", params: invalid_attributes.to_json, headers:headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Date can't be blank, Genre can't be blank/)
      end
    end
  end

  describe 'PUT /infos/:info_id' do
    let(:valid_attributes) { { city: "Porto Alegre" } }

    before { put "/infos/#{info_id}", params: valid_attributes.to_json, headers: headers}

    context 'when info exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the info' do
        updated_info = Info.find(info_id)
        expect(updated_info.city).to match(/Porto Alegre/)
      end
    end

    context 'when the info does not exist' do
      let(:info_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Info/)
      end
    end
  end

  describe 'DELETE /infos/:info_id' do
    before { delete "/infos/#{info_id}", headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
