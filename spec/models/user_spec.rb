require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:infos).dependent(:destroy) }
  it { should have_many(:accounts).dependent(:destroy) }
  it { should have_many(:transactions) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:cpf) }
  it { should validate_presence_of(:password_digest) }
end
