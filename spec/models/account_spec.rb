require 'rails_helper'

RSpec.describe Account, type: :model do
  it { should belong_to(:user)}
  it { should have_many(:transactions) }

  it { should validate_presence_of(:agency) }
  it { should validate_presence_of(:number) }
  it { should validate_presence_of(:balance) }
  it { should validate_presence_of(:limit) }
end
