FactoryBot.define do
  factory :info do
    city { Faker::Address.city }
    country { Faker::Address.country }
    date { Faker::Time.forward(23, :morning) }
    genre { "Female" }
  end
end
