FactoryBot.define do
  factory :transaction do
    transaction_type { random_transaction_type }
    value { Faker::Number.number(4) }
    
  end
end

def random_transaction_type
  random_number = Random.new
  array = ["Deposit", "Transfer", "Withdraw"]
  array[random_number.rand(3)]
end
