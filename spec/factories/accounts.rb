FactoryBot.define do
  factory :account do
    agency { Faker::Number.number(7) }
    number { Faker::Number.number(7) }
    balance { Faker::Number.decimal(2) }
    limit { Faker::Number.decimal(2) }
    limit_time { Faker::Time.between(DateTime.now - 2, DateTime.now - 2)}
  end
end
