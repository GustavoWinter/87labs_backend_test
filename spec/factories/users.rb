FactoryBot.define do
  factory :user do
    name { Faker::Name }
    cpf { Faker::Number.number(8) }
    password { 'a65s4a6sa1s654' }
  end
end
