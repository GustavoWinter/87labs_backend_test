ATM backend test
==================
Ruby version*
2.5.1*

Rails version*
5.2.0*

#Run the test suite

`bundle exec rails spec`

##Endpoints



**POST /auth/login**

*Authentication*


Authenticates an user.

- Required body parameters: cpf and password for user authentication.

- Returns the access-token, in case of success (200) or unauthorized (401) in case of invalid credentials.

- Return success (200) in case of success or not found (422) if the user isn't authenticated.


**POST /signup**

*User Registration*

Register a new user

- Required body parameters: name, cpf, password.

- Returns in case of success (200) or bad request (422) when missing required parameters or cpf is duplicate on the database.

**GET /users**

*Initial state*

Show all users.

- Required headers: access-token.

- Return success (200) in case of sucess or unauthorized (422) when the access-token is invalid.

**GET /users/:id**

Show the current user

- Required headers: access-token.

- Return success (200) in case of success or unauthorized (422) when the access-token is invalid.

**PUT /users/:id**

*Update User*

Update the current user params

- Required headers: access-token.
  
- Return success (200) in case of success or unauthorized (422) when the access-token is invalid.

**DELETE /users/:id**

*Delete User*

- Required headers: access-token.
  
- Return success (200) in case of success or unauthorized (422) when the access-token is invalid.
  
**POST /infos**

*Create User Infos*

Create a new infos table from the current user.

Required body parameters:

	{

	  city: "City",

	  country: "Country",

	  date: 12/12/1212,

	  genre: "Male"

	}

- Required headers: access-token

- Returns created (201) in case of success, Unprocessable Entity (422) when missing required parameters or unauthorized the authenticated user does not have the permission to create

**GET /infos**

Show all infos availables of the current user.

- Required headers: access-token.

- Return success (200) in case of success or unauthorized (422) when the access-token is invalid.

**GET /infos/:id**

Show the info by id.

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the info id.

**PUT /infos/:id**

*Update Info*

Update the info by id.

Required body: 

	{

	  city: "NewCity",

	  country: "NewCountry",

	  date: 10/10/1010,

	  genre: "Female"

	}

- Required headers: access-token.

- Return success (204) in case of success, unauthorized (422) when the access-token is invalid or Unprocessable Entity(422) when params is invalid.

**DELETE /infos/:id**

*Delete Info*

Delete a specific info by id.

- Required headers: access-token.

- Return success (204) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.

**POST /accounts**

*Create User Account*

Create a user account

- Required headers: access-token.

No need parameters, the fields are automatically genereted.

- Return accepted (201) when the account is created.

**GET /accounts**

Show all current user accounts

- Required headers: access-token.

Returns success (200) in case of success or unauthorized (422) when the access-token is invalid

**GET /accounts/:id**

Show a specific user account by id

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.

**GET /accounts/:account_id/bank_statement**

Show the bank statement from the last seven days of this specific user account.

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.

**GET /accounts/balance/:id**

Show the user account balance

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.

**PUT /accounts/:id**

The update is only available for the limit field. After make one request you have to wait 10 minutes to change the limit again.

- Required headers: access-token.

- Return success (204) in case of success,Accepted (202) when the limit time is not expired, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.


**DELETE /accounts/:id**

*Delete Account*

Delete a specific account by id.

- Required headers: access-token.

- Return success (204) in case of success, unauthorized (422) when the access-token is invalid or Not Found (404) when couldn't find the id.

**POST /transactions**

*Create Transaction*

Create a transaction that could be: Withdraw, Deposit or Transfer. They all have the same route, but a different transaction_type field.

  Create Transaction type - **Withdraw**

#First stage
Send the request to receive the possible combinations of notes.

Required body:

      
	{

		value: 100,

		transaction_type: "withdraw",

		from_agency: xxxx,

		account_id: x

	}
      
For the first stage the fields: from_agency and account_id are optional.

#Second stage
    
Send body with the same value from the first stage and add the note number you want.
      
Required body:

      
	{

		value: 100,

		transaction_type: "withdraw",

		rom_agency: xxxx,

		account_id: x,

	   note: y

	}

- Required headers: access-token.
  
- Return success (201) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params.

Create Transaction type - **Transfer**

Required body:



	{
	   value: 100,

	   transaction_type: "Transfer",

	   from_agency: xxxx,

	   account_id: x,

	}
    
For transfer the account id must be different as the agency.

- Required headers: access-token.
    
- Return success (201) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params and Not found (404) if the account id don't exists or couldn't be found.

Create Transaction type - **Deposit**

Required body:
    
    {
      value: 100,
      
      transaction_type: "Deposit",
      
      from_agency: xxxx,
      
      account_id: x,
      
    }
    
For deposit the account id must be the same as the agency.

- Required headers: access-token.
    
- Return success (201) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params and Not found (404) if the account id don't exists or couldn't be found.
    
**GET /transactions**

Show all user transactions

- Required headers: access-token.

- Return success (200) in case of success.

**GET /transactions/:id**

Show user transaction by id

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params and Not found (404) if the id don't exists or couldn't be found.


**GET /transactions/type/:transaction_type**

Show user transactions by type

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params and Not found (404) if the transaction is invalid.

**GET /transactions/type/:transaction_type/:account_id**

Show user transactions by type to a specific account id

- Required headers: access-token.

- Return success (200) in case of success, unauthorized (422) when the access-token is invalid or the request body doesn't have the valid params and Not found (404) if the id is invalid.


###Examples using  [HTTPie](https://httpie.org/)

**Create an user**

*Request*


		http POST :3000/signup name=UsuarioExample cpf=12345 password=123 password_confirmation=123

*Response*

		
		{

			"auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE1Mjg5MTE5ODd9.dzZU0nxCq6RockJZoTXsePM_8QIxTy1ZSEDxf3VEwlE", 

			"message": "Account created successfully"

		}

**Get accounts**

*Request*


	http :3000/accounts Authorization:eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE1Mjg5MTE5ODd9.dzZU0nxCq6RockJZoTXsePM_8QIxTy1ZSEDxf3VEwlE

**Create a transaction deposit type**


*Request*


	 http POST :3000/transactions value=200 transaction_type=deposit from_agency=3346 account_id=2 Authorization:eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0LCJleHAiOjE1Mjg5MTE5ODd9.dzZU0nxCq6RockJZoTXsePM_8QIxTy1ZSEDxf3VEwlE

	

